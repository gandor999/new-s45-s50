/*import Button from 'react-bootstrap/Button';
// Bootstrap grid system components
import Row from 'react-bootstrap/Row';
import Column from 'react-bootstrap/Column';
import { Row } from 'react-bootstrap';
import { Column } from 'react-bootstrap';*/

import Fragment from 'react';
import { Button, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner({data}){
	console.log(data)
	const {title, content, destination, label, isHome} = data;

	return (
		<Row>
			<Col className="p-5">
				<h1>{title}</h1>
				{/*<Link to={destination}>{label}</Link>
				<Button as={Link} to={destination} exact variant="primary">{label}</Button>*/}
				{(isHome ? 
					<div>
						<p>{content}</p>
					<Button as={Link} to={destination} exact variant="primary">{label}</Button>
					</div>
					:
					<p>{content}<Link to={destination}>{label}</Link></p>

				)}
			</Col>
		</Row>
	)
}
