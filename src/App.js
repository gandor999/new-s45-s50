// import { Fragment } from 'react';
import { useState, useEffect } from 'react';
import './App.css';
import AppNavbar from './components/AppNavbar'
import Home from './pages/Home';
import Courses from './pages/Courses';
import CourseView from './pages/CourseView';
import Register from './pages/Register';
import Login from './pages/Login';
import ErrorPage from './pages/Error';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Switch } from 'react-router-dom';
import Logout from './pages/Logout';
import { UserProvider } from './UserContext';





// function App() {
//   return (
//       <Fragment>
//         <AppNavbar />
//         <Container>
//           {/*<Home />
//           <Courses />*/}
//           {/*<Register />*/}
//           <Login />
//         </Container>
//       </Fragment>
//   );
// }

function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  }, [user])



  return (

    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar />
        <Container>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/courses" component={Courses} />
            <Route exact path="/courses/:courseId" component={CourseView} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/register" component={Register} />
            <Route exact path="/logout" component={Logout} />
            <Route component={ErrorPage} />
          </Switch>
        </Container>
      </Router>
    </UserProvider>
      
  );
}

export default App;
