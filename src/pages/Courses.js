import { Fragment, useEffect, useState } from 'react';
import CourseCard from '../components/CourseCard';
// import coursesData from '../data/coursesData';


// Initial notes before refactor by sir:

// export default function Courses(){
// 	console.log(coursesData)
// 	console.log(coursesData[0]);

// 	// Here we want to pass the coursesData object as an argument to the funciton CourseCard

// 	return (
// 		<Fragment>
// 			{/*We write the passing by assigning the object inside an attribute, in this case it is called courseProp*/}
// 			<CourseCard courseProp ={coursesData} />

// 			{/*So technically what is passed here is an attribute and not the object itself*/}
// 		</Fragment>
// 	)
// }




// Refactored code

export default function Courses(){
	// console.log(coursesData)
	// console.log(coursesData[0]);

	// State that will be used to store the courses retrieved from the database
	const [courses, setCourses] = useState([]);

	useEffect(() => {
		fetch('http://localhost:4000/courses/all')
		.then(res => res.json())
		.then(data => {
			console.log(data);

			// Sets the "courses" state to map the data retrieved from the fetch request in several "CourseCard" components
			setCourses(
				data.map(course => {
					return (
						<CourseCard key={course.id} courseProp = {course} />
					)
				})
			);
		})
	}, [])

	



	return (
		<Fragment>
			{courses}
		</Fragment>
	)
}